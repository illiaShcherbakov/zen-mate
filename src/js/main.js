import $ from 'jquery';



$(document).ready(()=>{

  $('#frlForm').submit((e) => {
    e.preventDefault();
    $('#frlResults').html('');
    const s = $('#frlInput').val();
    firstRepeatingLetter(s);
  });


  $('#fbForm').submit( (e) => {
    e.preventDefault();
    $('#fbResults').html('');
    const n = $('#fbInput').val();
    fizzBuzz(n);
  });
});

function fizzBuzz(n) {
  if ( n < 0 || n > 2 * Math.pow(10, 5) ) {
    console.log('Invalid input');
  } else {
    const b = n+1;
    for ( let i = 1; i <= n; i++) {
      if ( i % 3 == 0 ) {
        if ( i % 5 == 0 ) {
          $('#fbResults').append($('<p>FizzBuzz</p>') );
          console.log('FizzBuzz');
        } else {
          $('#fbResults').append($('<p>Fizz</p>'));
          console.log('Fizz');
        }
      } else if (i % 5 == 0) {
        $('#fbResults').append($('<p>Buzz</p>'));
        console.log('Buzz')
      } else {
        $('#fbResults').append($('<p>' + i +'</p>'));
        console.log(i);
      }
    }
  }
}

function firstRepeatingLetter(s) {
  if (s.length > 256) {
    console.log('Invalid input');
  } else {
    console.log(s);
    const sArr = s.split('').filter(entry => entry.trim() != '');
    console.log(sArr);
    let shouldBreak;
    for( let i = 0; i < sArr.length; i++ ) {
      for (let y = i+1; y < sArr.length - (i + 1) ; y++) {
        if (sArr[i] === sArr[y] ) {
          $('#frlResults').append($('<span>' + sArr[i] + ' <br></span>'));
          console.log(sArr[i]);
          shouldBreak = true;
          break;
        }
      }
      if ( shouldBreak === true ) {
        break;
      }
    }
  }
}

